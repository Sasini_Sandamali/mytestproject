* [Air Quality APIs](Air Quality APIs)
    * [Get Sensor Details](#1-get-active-device-count)
    * [Bulk Upload Credentials](#2-bulk-upload-credentials)
    * [Get Active Device Count](#3-get-active-device-count)
---

### **1. Get Sensor Details**

<table>
  <tr>
    <th> Title </th>
    <th> Description </th>
  </tr>
  <tr>
    <td> Endpoint </td>
    <td><code> https://api.airquality.lk/v1.0/sensors/</code> 
    </td>
  </tr>
  <tr>
    <td> Method </td>
    <td> GET </td>
  </tr>
  <tr>
    <td> Header </td>
    <td>
      <ul>
        <li> Content-Type &nbsp;: &nbsp; application/json </li> 
      </ul>
    </td>
  </tr>
  <tr>
    <td> Body </td>
    <td><code>meta_device_id,name,description,latitude,longitude,pm10,pm25,pm100,co2,temperature,timestamp
"NEA8000040","Nuwara Eliya","NEA8000040",6.951165,80.789604,8.0
,9.0,9.0,4996.0,32.02,1650104376187</code>
   </td>
  </tr>
</table>

---

### **2. Get Sensor Details by Sensor Id**

<table>
  <tr>
    <th> Title </th>
    <th> Description </th>
  </tr>
  <tr>
    <td> Endpoint </td>
    <td><code> https://api.airquality.lk/v1.0/sensors/{sensor-id} </code> 
    </td>
  </tr>
  <tr>
    <td> Method </td>
    <td> GET </td>
  </tr>
  <tr>
    <td> Header </td>
    <td>
      <ul>
        <li> Authorization &nbsp;: &nbsp; Bearer {{access_token}} </li>
        <li> Content-Type &nbsp;: &nbsp; application/json </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Query Params</td>
    <td>
      <ul>
        <li>sensor-id</li>
        <li>from</li>
        <li>to</li>
        <li>limit</li>
        <li>offset</li>
      </ul>
      Example: <br/>
      <code>https://api.airquality.lk/v1.0/sensors/NEA8000002389?from=1647262008663&to=1747262108663&limit=1&offset=2</code> 
    </td>
  </tr>
  </tr>
</table>

---

### **3. Get Active Device Count**

<table>
  <tr>
    <th> Title </th>
    <th> Description </th>
  </tr>
  <tr>
    <td> Endpoint </td>
    <td><code> https://api.airquality.lk/v1.0/sensors/active-count </code> 
    </td>
  </tr>
  <tr>
    <td> Method </td>
    <td> GET </td>
  </tr>
  <tr>
    <td> Header </td>
    <td>
      <ul>
        <li> Authorization &nbsp;: &nbsp; Bearer {{access_token}} </li>
        <li> Content-Type &nbsp;: &nbsp; text/plain </li>
      </ul>
    </td>
  </tr>
  <tr>
    <td>Query Params</td>
    <td>
      <ul>
        <li>duration</li>
        <li>threshold</li>
      </ul>
      Example: <br/>
      <code>https://api.airquality.lk/v1.0/sensors/active-count?duration=10000&threshold=20</code> 
    </td>
  </tr>
  </tr>
</table>

---